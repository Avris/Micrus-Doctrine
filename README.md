# Micrus Doctrine Bridge ##

This is a module for [Micrus framework](https://micrus.avris.it) that allows you
to integrate it with [Doctrine ORM](http://www.doctrine-project.org/projects/orm.html).

## Installation

Run:

    composer require avris/micrus-doctrine

Then register the module in your `App\App:registerModules`: 

    yield new \Avris\Micrus\Doctrine\DoctrineModule;

Finally, run:

    bin/env
    
You will be asked for a [connection string](http://docs.doctrine-project.org/projects/doctrine-dbal/en/latest/reference/configuration.html#connecting-using-a-url)
and it will be saved in the `.env` file.

## EntityManager

`DoctrineManager` will scan all modules for a `/src/Entity` directory and if it's there --
it will automatically register all classes from this directory as doctrine entities.  

Doctrine's `EntityManager` is registered in the container as `Doctrine\ORM\EntityManagerInterface`:

    public function incrementStuffAction(EntityManagerInterface $em)
    {
        $stuff = $em->getRepository('Stuff')->findBySomeCustomCondition();
        $stuff->incrementValue();
        $em->persist($stuff);
        $em->flush();
        $this->addFlash('success', 'Stuff incremented successfully');

        return $this->redirectToRoute('home');
    }

## UserProvider

This module registers Doctrine as the `UserProvider`.
It will try to find a user from `App\Entity\User` repository using `email` as identifier.
To modify those values, just use the container:

    Avris\Micrus\Doctrine\DoctrineUserProvider:
      arguments:
        $column: username

## MatchProvider

Doctrine will automatically match the arguments of your controller if they are entities, for example:

    /**
     * @M\Route("/post/{int:id}/read")
     */
    public function readAction(Post $post) { }
    
For request `GET /post/8/read` will try to find `App\Entity\Post` with `id = 8`
and either return it or throw a `NotFoundHttpException` if failed. 

## Console

Some Doctrine console commands are available in `bin/micrus` under `db` namespace:

    php bin/micrus db:schema:create
    php bin/micrus db:schema:update
    php bin/micrus db:schema:validate
    php bin/micrus db:schema:drop
     
    php bin/micrus db:query:sql "SELECT * FROM user"
    php bin/micrus db:query:dql "SELECT u FROM App\Model\User u WHERE u.role='ROLE_ADMIN'"

## Copyright

* **Author:** Andre Prusinowski [(Avris.it)](https://avris.it)
* **Licence:** [MIT](https://mit.avris.it)
