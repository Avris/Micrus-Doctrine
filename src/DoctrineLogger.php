<?php
namespace Avris\Micrus\Doctrine;

use Doctrine\DBAL\Logging\SQLLogger;
use Monolog\Logger;
use Psr\Log\LoggerInterface;

final class DoctrineLogger implements SQLLogger
{
    /** @var LoggerInterface */
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger instanceof Logger ? $logger->withName('doctrine') : $logger;
    }

    public function startQuery($sql, array $params = null, array $types = null)
    {
        $this->logger->notice($sql, $params ?: []);
    }

    public function stopQuery()
    {
    }
}
