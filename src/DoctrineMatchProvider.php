<?php
namespace Avris\Micrus\Doctrine;

use Avris\Micrus\Exception\Http\NotFoundHttpException;
use Avris\Micrus\Model\MatchProvider;
use Avris\Bag\QueueBag;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\MappingException;

final class DoctrineMatchProvider implements MatchProvider
{
    /** @var EntityManager */
    private $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    public function supports(\ReflectionParameter $parameter): bool
    {
        if (!$parameter->getClass()) {
            return false;
        }

        try {
            $this->em->getClassMetadata($parameter->getClass()->getName());

            return true;
        } catch (MappingException $e) {
            return false;
        }
    }

    public function fetch(\ReflectionParameter $parameter, QueueBag $tags)
    {
        if ($tags->isEmpty()) {
            return null;
        }

        $repo = $this->em->getRepository($parameter->getClass()->getName());

        list($key, $value) = $tags->dequeue();

        $entity = $repo->findOneBy([$key => $value]);

        if (!$entity) {
            throw new NotFoundHttpException(sprintf(
                'Cannot find an entity of type "%s" with %s=%s',
                $parameter->getClass()->getName(),
                $key,
                $value
            ));
        }

        return $entity;
    }
}
