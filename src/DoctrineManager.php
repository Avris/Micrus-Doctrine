<?php
namespace Avris\Micrus\Doctrine;

use Avris\Dispatcher\EventSubscriberInterface;
use Avris\Micrus\Bootstrap\ModuleInterface;
use Avris\Micrus\Console\ConsoleWarmupEvent;
use Doctrine\Common\Cache\ClearableCache;
use Doctrine\Common\Proxy\Autoloader;
use Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper;
use Doctrine\ORM\Cache;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper;
use Doctrine\ORM\Tools\Setup;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\Tools\Console\Command as ORMCommand;
use Doctrine\DBAL\Tools\Console\Command as DBALCommand;

final class DoctrineManager implements EventSubscriberInterface
{
    /** @var EntityManager */
    private $entityManager;

    public function __construct(
        string $envDatabaseUrl,
        bool $envAppDebug,
        string $proxyDir,
        array $modules,
        LoggerInterface $logger,
        DoctrineLogger $doctrineLogger,
        Cache $cache = null
    ) {
        $dirs = [];
        /** @var ModuleInterface $module */
        foreach (array_reverse($modules) as $module) {
            if (is_dir($dir = $module->getDir() . '/src/Entity')) {
                $dirs[] = $dir;
            }
        }

        $config = Setup::createAnnotationMetadataConfiguration($dirs, $envAppDebug, $proxyDir, $cache, false);
        $config->setSQLLogger($doctrineLogger);

        $this->entityManager = EntityManager::create(['url' => $envDatabaseUrl], $config);

        if ($warmupMessage = $this->warmupCache()) {
            $logger->notice($warmupMessage);
        }
    }

    public function getEntityManager()
    {
        return $this->entityManager;
    }

    public function clearCache()
    {
        $config = $this->entityManager->getConfiguration();

        $drivers = [
            $config->getQueryCacheImpl(),
            $config->getMetadataCacheImpl(),
            $config->getHydrationCacheImpl(),
        ];

        foreach ($drivers as $driver) {
            if ($driver instanceof ClearableCache) {
                $driver->deleteAll();
            }
        }
    }

    public function warmupCache()
    {
        $proxyDir = $this->entityManager->getConfiguration()->getProxyDir();

        if (file_exists($proxyDir)) {
            return false;
        }

        mkdir($proxyDir, 0777, true);
        $metadatas = $this->entityManager->getMetadataFactory()->getAllMetadata();
        $this->entityManager->getProxyFactory()->generateProxyClasses($metadatas, $proxyDir);

        return sprintf(
            'Metadata for %s generated in %s',
            implode(', ', array_map(function($el) { return $el->name; }, $metadatas)),
            $proxyDir
        );
    }

    public function registerCommands(ConsoleWarmupEvent $event)
    {
        $proxyDir = $this->entityManager->getConfiguration()->getProxyDir();
        Autoloader::register($proxyDir, 'DoctrineProxies');

        $app = $event->getApp();
        $helperSet = $app->getHelperSet();
        $helperSet->set(new ConnectionHelper($this->entityManager->getConnection()), 'db');
        $helperSet->set(new EntityManagerHelper($this->entityManager), 'em');

        $app->add((new DBALCommand\RunSqlCommand())->setName('db:query:sql'));
        $app->add((new ORMCommand\RunDqlCommand())->setName('db:query:dql'));

        $app->add((new ORMCommand\SchemaTool\CreateCommand())->setName('db:schema:create'));
        $app->add((new ORMCommand\SchemaTool\UpdateCommand())->setName('db:schema:update'));
        $app->add((new ORMCommand\SchemaTool\DropCommand())->setName('db:schema:drop'));
        $app->add((new ORMCommand\ValidateSchemaCommand())->setName('db:schema:validate'));
    }

    public function getSubscribedEvents(): iterable
    {
        yield 'consoleWarmup' => [$this, 'registerCommands'];
        yield 'cacheWarmup' => [$this, 'warmupCache'];
        yield 'cacheClear' => [$this, 'clearCache'];
    }
}
