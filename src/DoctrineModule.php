<?php
namespace Avris\Micrus\Doctrine;

use Avris\Micrus\Bootstrap\ModuleInterface;
use Avris\Micrus\Bootstrap\ModuleTrait;
use Avris\Micrus\Tool\Config\ParametersProvider;

final class DoctrineModule implements ModuleInterface, ParametersProvider
{
    use ModuleTrait;

    public function getParametersDefaults(): array
    {
        return [
            'DATABASE_URL' => 'mysql://root@127.0.0.1:3306/micrus?charset=utf8mb4',
        ];
    }
}
