<?php
namespace Avris\Micrus\Doctrine;

use Avris\Micrus\Model\User\UserProviderInterface;
use Avris\Micrus\Model\User\UserInterface;
use Doctrine\ORM\EntityManagerInterface;

final class DoctrineUserProvider implements UserProviderInterface
{
    /** @var EntityManagerInterface */
    private $em;

    /** @var string */
    private $entity;

    /** @var string */
    private $column;

    public function __construct(EntityManagerInterface $em, $entity = 'App\Entity\User', $column = 'email')
    {
        $this->em = $em;
        $this->entity = $entity;
        $this->column = $column;
    }

    public function getUser(string $identifier): ?UserInterface
    {
        return $this->em->getRepository($this->entity)
            ->findOneBy([$this->column => $identifier]);
    }
}
